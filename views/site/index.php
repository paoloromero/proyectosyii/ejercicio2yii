<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Selección II';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Selección 2</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 1</h3>
                        <p>Número de ciclistas que hay.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar1'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao1'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 2</h3>
                        <p>Número de ciclistas que hay del equipo Banesto.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar2'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao2'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar3'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao3'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            

        </div>
        
        <div class="row">
                
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 4</h3>
                        <p>La edad media de los del equipo Banesto.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar4'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao4'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 5</h3>
                        <p>La edad media de los ciclistas por cada equipo.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar5'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao5'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 6</h3>
                        <p>El número de ciclistas por equipo.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar6'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao6'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
                
        </div>
        
        <div class="row">
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 7</h3>
                        <p>El número total de puertos.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar7'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao7'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 8</h3>
                        <p>El número total de puertos mayores de 1500.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar8'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao8'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar9'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao9'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 10</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar10'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao10'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 11</h3>
                        <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar11'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao11'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        
                        <h3>Consulta 12</h3>
                        <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa.</p>
                        <p>
                            <?=Html::a('Active Record',['site/consultaar12'],['class'=>'btn btn-primary'])?>
                            <?=Html::a('DAO',['site/consultadao12'],['class'=>'btn btn-warning'])?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>

    </div>
</div>
